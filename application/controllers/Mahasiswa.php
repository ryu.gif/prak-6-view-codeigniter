<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

    public function index()
    {
        $data = [
            'todo' => [
                'Makan',
                'Ngoding',
                'Tidur',
            ],
            'nim' => 'M3118073',
            'nama' => 'Ramadhan Wahyu Indra Pradana'
        ];
        $this->load->view('mahasiswa', $data, FALSE);
    }

    public function cetak($nim = 'M31180xx', $nama = '')
    {
        $data = [
            'nim' => $nim,
            'nama' => $nama
        ];
        $this->load->view('mahasiswa', $data, FALSE);
    }

}

/* End of file Mahasiswa.php */

?>